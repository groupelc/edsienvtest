<?php
require 'database/db.php';
$users = getUsersWithRoles();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Page d'Administration</title>
    <link rel="stylesheet" href="css/admin.css">
</head>
<body>

<div class="container">
    <img src="images/wifi.png" alt="Logo WiFi" class="wifi-logo">
    <h1>Tableau de bord d'Administration</h1>

    <section class="user-list">
        <h2>Liste des Utilisateurs</h2>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Rôle</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= htmlspecialchars($user['id']) ?></td>
                    <td><?= htmlspecialchars($user['email']) ?></td>
                    <td><?= htmlspecialchars($user['role']) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</div>

</body>
</html>
