<?php
require 'functions.php';

session_start();

$error = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $error = connexion($email, $password);

    if (!$error) {
        $userId = $_SESSION['user_id'];

        updateLastConn($userId);
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Connexion</title>
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<div class="container">
    <img src="images/wifi.png" alt="wifi Logo" class="logo">
    <h2>Formulaire de Connexion</h2>

    <?php if ($error): ?>
        <p style="color: red;"><?php echo $error; ?></p>
    <?php endif; ?>

    <form action="index.php" method="post">
        <label for="email">Email :</label>
        <input type="email" name="email" id="email" required>
        <br>
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" id="password" required>
        <br>
        <button type="submit">Connexion</button>
    </form>

    <p>Vous n'avez pas de compte ? <a href="registration.php">Inscrivez-vous ici</a>.</p>
    <p>En vous connectant, vous acceptez nos <a href="conditionOfUse.php">conditions d'utilisation</a>.</p>
</div>
</body>
</html>
