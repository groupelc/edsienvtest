<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion WiFi réussie</title>
    <link rel="stylesheet" href="css/user.css">
</head>
<body>
    <div class="wifi-success">
        <img src="images/wifi.png" alt="Logo WiFi" class="wifi-logo">
        <h1>Vous êtes maintenant connecté au réseau WiFi public de Wideconnect !</h1>
        <p>Bienvenue sur notre réseau. Naviguez en toute sécurité et librement.</p>
        <p><a href="conditionOfUse.php">Conditions d'utilisation</a></p>
    </div>
</body>
</html>
