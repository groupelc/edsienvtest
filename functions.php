<?php
require 'database/db.php';

function connexion($email, $password) {
    $user = verifyUser($email, $password);

    if ($user) {
        if (!isset($_SESSION)) {
            session_start();
        }

        $_SESSION['user_id'] = $user['id'];

        $_SESSION['role'] = getUserRole($user['id']);

        updateLastConn($user['id']);

        if ($_SESSION['role'] === 'admin') {
            header("Location: administration.php");
        } else {
            header("Location: user.php");
        }
        exit;
    } else {
        return 'Identifiants de connexion invalides';
    }
}
?>
