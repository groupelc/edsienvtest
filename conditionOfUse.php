<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conditions d'utilisation du réseau WiFi public</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        h1 {
            text-align: center;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
        }
        p {
            line-height: 1.6;
        }
        .footer {
            margin-top: 50px;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Conditions d'utilisation du réseau WiFi public Wideconnect</h1>
    <p>
        Bienvenue sur notre réseau WiFi public. En utilisant ce service, vous acceptez les conditions suivantes :
    </p>
    <ol>
        <li>
            <strong>Utilisation responsable :</strong> Vous êtes responsable de votre utilisation du réseau WiFi public. Ne faites rien qui puisse compromettre la sécurité ou la disponibilité du réseau.
        </li>
        <li>
            <strong>Protection des données :</strong> N'utilisez pas le réseau pour accéder à des informations sensibles ou personnelles, sauf si vous avez pris des mesures appropriées pour sécuriser votre connexion.
        </li>
        <li>
            <strong>Respect de la loi :</strong> Respectez toutes les lois et réglementations locales, nationales et internationales en utilisant ce réseau.
        </li>
        <li>
            <strong>Temps d'utilisation :</strong> La durée de votre utilisation peut être limitée. Assurez-vous de respecter les règles d'utilisation du réseau.
        </li>
    </ol>
    <p>
        En utilisant ce réseau WiFi, vous acceptez de vous conformer à ces conditions. Tout abus du réseau peut entraîner la suspension ou la résiliation de votre accès.
    </p>
</div>
<div class="footer">
    <p>© 2024 Tous droits réservés. Wideconnect.</p>
</div>
</body>
</html>
