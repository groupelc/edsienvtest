<?php
$pdo = new PDO('mysql:host=127.0.0.1;dbname=wideconnect', 'root', '');

function verifyUser($email, $password) {
    global $pdo;

    $stmt = $pdo->prepare("SELECT * FROM users WHERE email = :email");
    $stmt->execute(['email' => $email]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    //la connexion à réussi
    if ($user && password_verify($password, $user['password'])) {
        return $user;
    }
    return false;
}

function updateLastConn($userId) {
    global $pdo;

    $stmt = $pdo->prepare("UPDATE users SET last_conn = NOW() WHERE id = :user_id");
    $stmt->execute(['user_id' => $userId]);
}

function createUser($email, $password) {
    global $pdo;

    // Vérifier si l'utilisateur existe déjà
    $stmt = $pdo->prepare("SELECT * FROM users WHERE email = :email");
    $stmt->execute(['email' => $email]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($user) {
        return false;
    }

    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    $stmt = $pdo->prepare("INSERT INTO users (email, password, last_conn, role_id) VALUES (:email, :password, NOW(), :role_id)");
    $success = $stmt->execute([
        'email' => $email,
        'password' => $hashedPassword,
        'role_id' => 2
    ]);

    return $success;
}

function getUserRole($userId) {
    global $pdo;

    $stmt = $pdo->prepare("SELECT roles.role FROM users JOIN roles ON users.role_id = roles.id WHERE users.id = :user_id");
    $stmt->execute(['user_id' => $userId]);
    $role = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($role) {
        return $role['role'];
    }
    return false;
}

function getUsersWithRoles() {
    global $pdo;

    $stmt = $pdo->prepare("SELECT users.id, users.email, roles.role FROM users JOIN roles ON users.role_id = roles.id");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
