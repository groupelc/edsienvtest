<?php
require 'functions.php';
session_start();

$error = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $passwordConfirm = $_POST['passwordConfirm'];

    if ($password !== $passwordConfirm) {
        $error = 'Les mots de passe ne correspondent pas.';
    } else {
        $creationSuccess = createUser($email, $password);

        if ($creationSuccess) {
            $error = connexion($email, $password);
        } else {
            $error = 'Erreur lors de la création de l\'utilisateur. L\'email est peut-être déjà utilisé.';
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Inscription</title>
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<div class="container">
    <img src="images/wifi.png" alt="wifi Logo" class="logo">
    <h2>Formulaire d'Inscription</h2>

    <?php if ($error): ?>
        <p style="color: red;"><?php echo $error; ?></p>
    <?php endif; ?>

    <form action="registration.php" method="post">
        <label for="email">Email :</label>
        <input type="email" name="email" id="email" required>
        <br>
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" id="password" required>
        <br>
        <label for="passwordConfirm">Confirmez le mot de passe :</label>
        <input type="password" name="passwordConfirm" id="passwordConfirm" required>
        <br>
        <button type="submit">S'inscrire</button>
    </form>

    <p>En vous inscrivant, vous acceptez nos <a href="conditionOfUse.php">conditions d'utilisation</a>.</p>

</div>
</body>
</html>
